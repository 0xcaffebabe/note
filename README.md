# 个人知识框架

来源于平时学习/工作，包含 算法与数据结构 / 操作系统 / 数据库 / 计算机系统 / 网络 / 中间件 / Java / 软件工程 / 前端 / 运维 / 网络安全

在线阅读:

<https://note.ismy.wang>(github pages, cf加速)

<https://b.ismy.wang>(国内服务器)

<https://notec.ismy.wang>(cloudflare pages)

<https://notev.ismy.wang>(vercel app, 比github pages快)

- [参考文献](./参考文献.md)

--------------------------------------------------------------------------------

[![wakatime](https://wakatime.com/badge/github/0xcaffebabe/note.svg)](https://wakatime.com/badge/github/0xcaffebabe/note) ![字数](https://cdn.jsdelivr.net/gh/0xcaffebabe/note@gh-pages/wordCountBadge.svg) ![star](https://img.shields.io/github/stars/0xcaffebabe/note) ![fork](https://img.shields.io/github/forks/0xcaffebabe/note) ![license](https://img.shields.io/github/license/0xcaffebabe/note) ![build](https://github.com/0xcaffebabe/note/workflows/%E6%9E%84%E5%BB%BA%E7%94%B5%E5%AD%90%E4%B9%A6/badge.svg)

![统计](https://repobeats.axiom.co/api/embed/24137e8c365c058184db40c146a5dc1291924862.svg "Repobeats analytics image")

---

知识体系应该是什么样的？基础深厚，涉猎广泛，融汇贯通

![2022525213243](/doc/assets/2022525213243.webp)
![2022525213254](/doc/assets/2022525213254.webp)

## [关于知识框架](/doc/MyBook.md)
