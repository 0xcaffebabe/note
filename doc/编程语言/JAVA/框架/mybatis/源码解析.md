# Mybatis源码解析

读源码技巧：

1. 猜（如果让你设计 你会如何设计）
2. 调试（带着问题 具体看某一操作如何实现）
3. 验证
4. 整体框架过一遍
5. 细节不必太纠结


核心接口：

- SqlSession
- SqlSessionFactory
- ResultHandler

XML相关：

- XMLConfigBuilder：读取XML配置文件
  - XPathParser：根据XPath解析

重点：

- MapperRegistry：负责Mapper接口处理
  - MapperAnnotationBuilder
- Configuration

MapperProxy:

- invoke

MapperMethod

ParamNameResolver

MappedStatement

Executor

StatementHandler

MetaObject：拦截器元数据

## 关于Mapper方法重载

对于两个拥有相同方法名，但入参不同的方法，Mybatis会如何处理

1. 如果采用传统的xml xml必须指定一个id 当在启动时 就会报这个id冲突了
2. 但如果不用xml 用的是注解呢? 这个问题我的意图是想调用第一个双参方法，却发现偶尔成功，偶尔报错 提示 Paramter not found

```java
@Select("sql xxx")
Long statisticTotal(@Param("beginTime") String beginTime, @Param("endTime") String endTime);

@Select("sql xxx")
Long statisticTotal(@Param("name") String name);
```

在运行时，每个Mapper的方法调用最终都会被一个代理所捕获

```java
// 在Mybatis中是在org.apache.ibatis.binding.MapperProxy
// MyBatisPlus中是在com.baomidou.mybatisplus.core.override.MybatisMapperProxy
@Override
public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
        if (Object.class.equals(method.getDeclaringClass())) {
            return method.invoke(this, args);
        } else {
            return cachedInvoker(method).invoke(proxy, method, args, sqlSession);
        }
    } catch (Throwable t) {
        throw ExceptionUtil.unwrapThrowable(t);
    }
}
```

在cachedInvoker方法中，其会将接口的方法进行包装为DefaultMethodInvoker或者PlainMethodInvoker

```java
return methodCache.computeIfAbsent(method, m -> {
  if (m.isDefault()) {
    try {
      if (privateLookupInMethod == null) {
        return new DefaultMethodInvoker(getMethodHandleJava8(method));
      } else {
        return new DefaultMethodInvoker(getMethodHandleJava9(method));
      }
    } catch (IllegalAccessException | InstantiationException | InvocationTargetException
        | NoSuchMethodException e) {
      throw new RuntimeException(e);
    }
  } else {
    return new PlainMethodInvoker(new MapperMethod(mapperInterface, method, sqlSession.getConfiguration()));
  }
});
```

其中DefaultMethodInvoker是为了实现默认方法的调用而实现的，重点还是PlainMethodInvoker，其创建时又需要传递一个 MapperMethod

这个MapperMethod就是在cachedInvoker后invoke的方法，在MyBatisPlus这个类是com.baomidou.mybatisplus.core.override.MybatisMapperMethod

在MapperMethod创建时，会将接口的方法转为一条命令

```java
this.command = new MapperMethod.SqlCommand(config, mapperInterface, method);
```

其内部就是根据Mapper接口名称以及方法在配置中寻找对应的Statement

并且在resolveMappedStatement 方法中，它生成了一个statementId 就是用来寻找Statement:

```java
// MapperMethod
String statementId = mapperInterface.getName() + "." + methodName;
```

由此可见，参数并不构成这个唯一ID，至此Command的内容就差不多清楚了 而且这个statementId 就是command的name

```java
// MapperMethod.SqlCommand
name = ms.getId();
type = ms.getSqlCommandType();
```

回到MapperMethod的execute（MyBatisPlus中是invoke） 代理捕获调用后来到这里 根据先前的command的name去调用sqlSession

而sqlSession则根据这个name去查找MappedStatement，再根据MappedStatement以及传递过来的参数构造SQL语句，最后发到数据库去查询

```java
// org.apache.ibatis.session.defaults.DefaultSqlSession
MappedStatement ms = configuration.getMappedStatement(statement);
return executor.query(ms, wrapCollection(parameter), rowBounds, Executor.NO_RESULT_HANDLER);

// org.apache.ibatis.executor.BaseExecutor
BoundSql boundSql = ms.getBoundSql(parameter);
CacheKey key = createCacheKey(ms, parameter, rowBounds, boundSql);
return query(ms, parameter, rowBounds, resultHandler, key, boundSql);
```

那么最后还有一个问题，就是两个同名的方法存在时，我去调用其中一个，却时好时坏，我的猜测是这个MappedStatement的加载顺序是不固定的，相同id的Statement会导致后面扫描到的覆盖之前的，但如果这样的话，为什么有时行有时不行？

但是在刚才的调用链路中似乎并没有发现是如何加载Statement的，于是换个思路，从SpringBoot的自动装配来着手

在MybatisPlus starter的jar包下，有个spring.factories的文件 里面记录了要自动装配哪些类

当然就发现了com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration 里面有个内部类：com.baomidou.mybatisplus.autoconfigure.MybatisPlusAutoConfiguration.AutoConfiguredMapperScannerRegistrar 看名字就是这个了

这个类声明一大堆对Mapper的描述为BeanDefinition 并交由org.mybatis.spring.mapper.MapperScannerConfigurer来进行扫描 经过一顿扫描 这里没怎么读懂把Configuration注入到Spring的，我猜大概是利用了Spring的一些机制，最后是会调用到

```java
configuration.addMapper(this.mapperInterface);
```

最后来到org.apache.ibatis.binding.MapperRegistry （MyBatisMapperRegistry）

这里会对每个Mapper接口的方法进行处理

```java
// addMapper
MybatisMapperAnnotationBuilder parser = new MybatisMapperAnnotationBuilder(config, type);
parser.parse();
// 对Mapper接口的每个方法做处理
for (Method method : type.getMethods()) {
  if (!canHaveStatement(method)) {
      continue;
  }
  if (getAnnotationWrapper(method, false, Select.class, SelectProvider.class).isPresent()
      && method.getAnnotation(ResultMap.class) == null) {
      parseResultMap(method);
  }
  try {
      // TODO 加入 注解过滤缓存
      InterceptorIgnoreHelper.initSqlParserInfoCache(cache, mapperName, method);
      parseStatement(method);
  } catch (IncompleteElementException e) {
      // TODO 使用 MybatisMethodResolver 而不是 MethodResolver
      configuration.addIncompleteMethod(new MybatisMethodResolver(this, method));
  }
}
```

重点就在于type.getMethods 这个地方，随着我每次启动JVM，每次获取的methods返回的数组顺序是不一样的，这就导致我之前那个时好时坏的问题，后面的相同方法名覆盖了之前的，但是谁先谁后并不是一个确定性的行为

Class.getMethods的文档：

```text
The elements in the returned array are not sorted and are not in any particular order
```

调试直到这里，也是印证了我的猜测，不过问题不出在MyBatis，而是出在JDK的反射机制上
